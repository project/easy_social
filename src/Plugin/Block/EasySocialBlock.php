<?php

namespace Drupal\easy_social\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Component\Annotation\Plugin;
use Drupal\Core\Annotation\Translation;

/**
 * Provides an Easy Social block.
 *
 * @Block(
 *   id = "easy_social_block",
 *   admin_label = @Translation("Easy Social"),
 * )
 */
class EasySocialBlock extends BlockBase {

  /**
   * Implements \Drupal\Block\BlockBase::blockBuild().
   */
  public function build() {
    $content = [
      '#theme' => 'easy_social',
    ];

    return [
      '#children' => \Drupal::service('renderer')->render($content),
    ];
  }

}
